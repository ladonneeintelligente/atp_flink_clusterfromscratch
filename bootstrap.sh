#!/usr/bin/env bash

cp /vagrant/hosts /etc/hosts
cp /vagrant/resolv.conf /etc/resolv.conf
ONBOOT=no
dhclient
yum update -y
# yum install ntp -y // replaced by chrony installed by default in centos 8 systemctl status chronyd
service iptables stop
mkdir -p /root/.ssh; chmod 700 /root/.ssh; cp /home/vagrant/.ssh/authorized_keys /root/.ssh/

#Again, stopping iptables
/etc/init.d/iptables stop

# Increasing swap space
sudo dd if=/dev/zero of=/swapfile bs=1024 count=1024k
sudo mkswap /swapfile
sudo swapon /swapfile
echo "/swapfile       none    swap    sw      0       0" >> /etc/fstab

sudo cp /vagrant/insecure_private_key /root/ec2-keypair
sudo chmod 600 /root/ec2-keypair

yum install java-1.8.0-openjdk -y

#download Flink
yum install wget -y
cd /opt
wget https://archive.apache.org/dist/flink/flink-1.11.2/flink-1.11.2-bin-scala_2.12.tgz
sudo tar -xvzf flink-1.11.2-bin-scala_2.12.tgz

cp /vagrant/flink-conf.yaml /opt/flink-1.11.2/conf/flink-conf.yaml


